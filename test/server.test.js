var env = process.env.NODE_ENV || 'test';
require('dotenv').load();

var should = require('should');
var request = require('supertest');
var mongoose = require('mongoose');
var express = require('express');
var app = express();
var config = require('../app/config');
var mockServer = require('../app/server');

var baseUrl = 'http://localhost:8888';


//Set up Test Env before each test
beforeEach(function(done) {

  function clearDB() {
    for (var i in mongoose.connection.collections) {
      mongoose.connection.collections[i].remove();
    }
    return done();
  }

  function reconnect() {
    mongoose.connect(config.mongo_url(env), function(err) {
      if (err) {
        throw err;
      }
      return clearDB();
    });
  }

  function checkState() {
    switch (mongoose.connection.readyState) {
      case 0:
        reconnect();
        break;
      case 1:
        clearDB();
        break;
      default:
        process.nextTick(checkState);
    }
  }

  checkState();



});


// Clean up after each test
afterEach(function(done) {
  mongoose.disconnect();
  return done();
});



//========================= TEST BEGINS ====================================

describe('MockServer', function() {

  request = request(baseUrl);

  it('should be ready for testing', function(done) {

    request.get('/')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);

        should.not.exist(err);

        done();
      });
  });


  it('should have access to 0, 20, 40, 60', function(done) {

    request.get('/b/l/restaurant/Auckland/0')
      .expect(200)
      .expect('Content-Type', /html/)
      .end(function(err, res) {
        if (err) return done(err);
      });

    request.get('/b/l/restaurant/Auckland/20')
      .expect(200)
      .expect('Content-Type', /html/)
      .end(function(err, res) {
        if (err) return done(err);
      });

    request.get('/b/l/restaurant/Auckland/40')
      .expect(200)
      .expect('Content-Type', /html/)
      .end(function(err, res) {
        if (err) return done(err);
      });

    request.get('/b/l/restaurant/Auckland/60')
      .expect(200)
      .expect('Content-Type', /html/)
      .end(function(err, res) {
        if (err) return done(err);
      });

    request.get('/b/l/restaurant/Auckland/')
      .expect(404)
      .expect('Content-Type', /html/)
      .end(function(err, res) {
        if (err) return done(err);
      });

    request.get('/b/l/restaurant/Auckland/80')
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);

        done();
      });

  });


  it('should', function(done) {

    request.get('/')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);

        should.not.exist(err);

        done();
      });
  });


});









//