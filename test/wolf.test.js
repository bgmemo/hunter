var env = process.env.NODE_ENV || 'test';
require('dotenv').load();
var Q = require('q');
var should = require('should');
var request = require('supertest');
var mongoose = require('mongoose');
var express = require('express');
var app = express();
var config = require('../app/config');
var Wolf = require('../app/wolf');


//Set up Test Env before each test
beforeEach(function(done) {

  function clearDB() {
    // for (var i in mongoose.connection.collections) {
    //   mongoose.connection.collections[i].remove(function(err) {
    //     if (err) {
    //       throw err;
    //     }
    //   });
    // }
    return done();
  }

  function reconnect() {
    mongoose.connect(config.mongo_url(env), function(err) {
      if (err) {
        throw err;
      }
      return clearDB();
    });
  }

  function checkState() {
    switch (mongoose.connection.readyState) {
      case 0:
        reconnect();
        break;
      case 1:
        clearDB();
        break;
      default:
        process.nextTick(checkState);
    }
  }

  checkState();
});



// Clean up after each test
afterEach(function(done) {
  mongoose.disconnect();
  return done();
});



//========================= TEST BEGINS ====================================


describe('MockServer', function() {

  request = request('http://localhost:8888');

  it('should be up for testing', function(done) {

    request.get('/b/l/restaurant/Auckland/10').expect(404);

    request.get('/b/l/restaurant/Auckland/0')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);

        res.should.be.html;

        done();
      });
  });



})




describe('Wolf', function() {


  it('should get correct prey\'s size', function(done) {

    Wolf.isFull(20).should.be.true;
    Wolf.isPartial(3).should.be.true;
    Wolf.isEmpty(0).should.be.true;

    done();
  });



  it('should return correct next path', function(done) {

    var page_0 = new Wolf.NextPath();
    page_0.getPath(true).should.equal('/b/l/restaurant/Auckland/20');
    var page_1 = new Wolf.NextPath();
    page_1.getPath(true).should.equal('/b/l/restaurant/Auckland/40');
    var page_2 = new Wolf.NextPath();
    page_2.getPath(true).should.equal('/b/l/restaurant/Auckland/60');

    done();
  });



  // it('should sniff correct number of preys', function(done) {

  //   Wolf.hunt('/b/l/restaurant/Auckland/0');
  //   Wolf.prey.should.equal(0);
  //   done();
  // });



  it('should return one geo result', function(done) {

    var json;

    Wolf.geo('112 kyle, rd, albany').then(function(res) {

      json = res;

    }).done(function() {

      var results = json.results;

      try {
        Wolf.sortGeo(results);

        json.status.should.equal('OK');
        json.results.length.should.equal(1);

        done();
      } catch (x) {
        done(x);
      }
    });
  })




  it('should sort shop correctly', function(done) {

    var json;

    var shopVars = {
      name: 'a',
      phone: '09-888-8888',
      _category: '',
      address: '112 kyle rd, albany',
      geo: {
        loc: {
          type: 'Point',
          coordinates: [0, 0]
        }
      }
    };
    
    try {
      Wolf.sortShop(shopVars).then(function(vars) {
        json = vars;
      }).done(function() {

        console.log(json);
      })
      done();
    } catch (x) {
      done(x);
    }

  })








})







//