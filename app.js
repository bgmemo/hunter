/**
 *
 */
var env = process.env.NODE_ENV || 'development';
require('dotenv').load();
var Q = require('q');
var mongoose = require('mongoose');
var config = require('./app/config');
var Pard = require('./app/pard.js');
mongoose.connect(config.mongo_url(env));

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error.'));
db.once('open', function() {
  console.log('db opened! path: ' + __dirname);

  Pard.host = process.env.CRAWLER_TARGET_HOST;
  Pard.port = 80;
  Pard.basePath = process.env.CRAWLER_TARGET_PATH; //No num
  Pard.headers = {
    'User-Agent': process.env.CRAWLER_HEADER
  };

  Pard.sniff(Pard.basePath + '0');
});