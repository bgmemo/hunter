/**
 *
 * This is the configures of express and mongoDB.
 *
 * To-dos: Config password using arguments !important.
 *
 */

exports.mongo_url = function(env) {

  var mongo = {};

  var MONGOURI = process.env.MONGOURI;

  switch (env) {
    case 'development':
      mongo.url = MONGOURI;
      break;

    case 'test':
      mongo.url = MONGOURI;
      break;

    case 'production':
    default:
      mongo.url = MONGOURI;
      break;
  }

  return mongo.url;
}



exports.session_url = function(env) {

  var mongo = {};

  switch (env) {
    case 'development':
      mongo.session_url = process.env.MONGOURI;
      break;

    case 'test':
      mongo.session_url = process.env.MONGOURI;
      break;

    case 'production':
    default:
      mongo.session_url = process.env.MONGOURI;
      break;
  }

  return mongo.session_url;
}