/**
 * ===================
 * Model of Shop
 * ===================
 *
 *
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CREATEDBY = 'SYSTEM ADMIN OWNER USER'.split(' ');

var ShopSchema = new Schema({
  name: {
    type: String,
    trim: true
  },
  address: {
    type: String,
    trim: true
  },
  geo: {
    street: {
      type: String
    },
    route: {
      type: String
    },
    district: {
      type: String
    },
    city: {
      type: String
    },
    zipcode: {
      type: String
    },
    loc: {
      'type': {
        type: String
      },
      'coordinates': []
    }
  },
  phone: {
    type: String,
    trim: true
  },
  mobile: {
    type: String,
    trim: true
  },
  website: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    trim: true
  },
  desc: {
    type: String,
    trim: true
  },
  _category: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  },
  _shop: {
    type: Schema.Types.ObjectId,
    ref: 'Shop'
  },
  approved: {
    type: Boolean,
    default: false
  },
  created_by: {
    type: String,
    default: 'USER',
    enum: CREATEDBY
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  creator_ip: {
    type: String,
    trim: true
  }
});


mongoose.model('TmpShop', ShopSchema);

module.exports = mongoose.model('TmpShop');