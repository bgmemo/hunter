/**
 * ===================
 * Model of Category
 * ===================
 * 
 * parent - The parent category's _id;
 * children - A collection of its children categories' _id;
 * shops - A collection of shops falling into the category;
 *
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// mongoose.models = {};
// mongoose.modelSchemas = {};

var CategorySchema = new Schema({
  name: {
    type: String,
    trim: true
  },
  _parent: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  },
  _ancestors: [{
    type: Schema.Types.ObjectId,
    ref: 'Category'
  }]
});

mongoose.model('Category', CategorySchema);

module.exports = mongoose.model('Category');

