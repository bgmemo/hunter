'use strict';

var http = require('http');
var request = require('request');
var _ = require('lodash-node');
var C = require('cheerio');
var Q = require('q');
var S = require('string');
var V = require('validator');
var iconv = require('iconv-lite');

var ShopModel = require('/app/models/shop');
var TmpShopModel = require('models/tmpshop');
var CategoryModel = require('models/category');

exports.prey = 0;


//Defalt http options for mocha testing
exports.host = 'localhost';
exports.port = 8888;
exports.basePath = '/b/l/restaurant/Auckland/'; //No num
exports.headers = {
  'User-Agent': 'Mozilla/6.0 (compatible; MSIE 7.0; Windows NT 6.0)'
}



exports.NextPath = (function() {

  var i = 0,
    Next;

  Next = function() {
    i += 20;
  }

  Next.prototype.getPath = function(hasNext) {

    var path = (hasNext) ? (exports.basePath + i) : '';

    return path;
  }

  return Next;
}());




exports.next = function(hasNext) {

  var nextPath = new exports.NextPath();

  var interval = Math.random() * 1000;

  setTimeOut(exports.sniff(nextPath.getPath(hasNext), exports.next), interval);
}




exports.hunt = function() {

  exports.sniff(exports.basePath + '0', function(hasNext) {

    var nextPath = new exports.NextPath();

    exports.sniff(nextPath.getPath(hasNext), exports.next);
  })
}




exports.sniff = function(currentPath, next) {

  if (!currentPath) {

    exports.stop();

  } else {

    exports.digest(currentPath).then(function(num) {

      exports.prey += num;

      console.log('exports.prey: ' + exports.prey);

      if (exports.isFull(num)) {
        next(true)
      } else {
        next(false)
      }
    });


  }
}




exports.digest = function(currentPath) {

  var options = {
    host: exports.host,
    port: exports.port,
    path: currentPath,
    headers: exports.headers
  };

  var deferred = Q.defer();

  exports.getChunkHtml(options).then(function(html) {

    var $ = C.load(html);
    var targetBlock = exports.shopDivs($);

    exports.parseSave($).then(function(num) {

      deferred.resolve(num);
    });
  })

  return deferred.promise;
}





exports.stop = function() {
  if (process.env.NODE_ENV === 'production') {
    process.exit(1);
  }
}



exports.isFull = function(num) {
  var isFull = (num === 20) ? true : false;
  return isFull;
}



exports.isPartial = function(num) {
  var isPartial = (num < 20) ? true : false;
  return isPartial;
}



exports.isEmpty = function(num) {
  var isEmpty = (num === 0) ? true : false;
  return isEmpty;
}





exports.getChunkHtml = function(options) {

  var deferred = Q.defer();

  // deferred.resolve(exports.html);

  http.get(options, function(res) {
    var size = 0;
    var chunks = [];
    res.on('data', function(chunk) {
      size += chunk.length;
      chunks.push(chunk);
    });
    res.on('end', function() {

      var data = Buffer.concat(chunks, size);

      var html = iconv.decode(data, 'utf-8'); //gb2312

      deferred.resolve(html);

    });
  }).on('error', function(e) {
    console.log(e);
  });

  return deferred.promise;
}





exports.shopDivs = function($) {

  var targetBlock = $('ul.result.fL').children();

  return targetBlock;
}





exports.parseSave = function($) {

  var def = Q.defer();
  var numShops = 0;

  var targetBlock = exports.shopDivs($);

  var name = targetBlock.first().children('a').first().find('span').text().substring(1, 3);

  CategoryModel.findOne({
    name: name
  }).exec().then(function(category) {

    var deferred = Q.defer();

    if (category) {

      deferred.resolve(category);
    } else {

      var model = new CategoryModel();
      model.name = name;
      model.save(function(err, category) {
        deferred.resolve(category);
      });
    }

    return deferred.promise;

  }).then(function(category) {

    targetBlock.each(function(i, el) {

      if ($(this).is('li') && !$(this).hasClass('counter')) {

        parseShop($(this), category).then(function(vars) {
          saveShop(vars);
        })

        numShops++;
      }
    })

    def.resolve(numShops);
  });

  return def.promise;
}





var parseShop = function(elem, category) {

  var deferred = Q.defer();

  var aList = elem.children('a');

  var shopName = aList.first().find('b').text();
  var shopPhone = V.whitelist(aList.last().text(), '0123456789');
  var shopCategory = category._id;

  aList.remove(); // For extracting address

  var shopAddress = S(elem.text()).humanize().s.substring(7);

  if (shopAddress.length > 18) {

    var shopVars = {
      name: shopName,
      phone: phoneFormat(shopPhone),
      _category: shopCategory,
      address: shopAddress,
      geo: {
        loc: {
          type: 'Point',
          coordinates: [0, 0]
        }
      }
    };

    exports.sortShop(shopVars).then(function(vars) {
      deferred.resolve(vars);
    })

  } else {
    console.log("shopVars.length: " + shopAddress.length);
    deferred.resolve('');
  }

  return deferred.promise;
}





exports.sortShop = function(vars) {

  var deferred = Q.defer();

  exports.geo(vars.address).then(function(json) {

    var resultLength = json.results.length;

    if (resultLength > 1) {

      deferred.resolve(vars);
    } else {
      var shopVars = vars;

      var geos = exports.sortGeo(json.results);
      var geo = geos[0];
      var cord = [];
      cord.push(parseFloat(geo.lng));
      cord.push(parseFloat(geo.lat));

      shopVars.address = geo.street + ' ' + geo.route + ', ' + geo.district + ', ' + geo.city;

      shopVars.geo.street = geo.street;
      shopVars.geo.route = geo.route;
      shopVars.geo.district = geo.district;
      shopVars.geo.city = geo.city;
      shopVars.geo.loc.coordinates = cord;

      shopVars.isNotTemp = true;

      deferred.resolve(shopVars);
    }
  })

  return deferred.promise;
}






exports.sortGeo = function(results) {

  var geos = [];

  _(results).forEach(function(result) {

    var geo = {};

    var components = result.address_components;

    geo.lat = result.geometry.location.lat;

    geo.lng = result.geometry.location.lng;

    _(components).forEach(function(component) {

      var types = component.types;

      if (_.contains(types, 'street_number')) {

        geo.street = component.long_name;

      } else if (_.contains(types, 'route')) {

        geo.route = component.short_name;

      } else if (_.contains(types, 'sublocality')) {

        geo.district = component.long_name;

      } else if (_.contains(types, 'locality')) {

        geo.city = component.long_name;

      } else if (_.contains(types, 'postal_code')) {

        geo.zipcode = component.long_name;

      } else {}
    })

    geos.push(geo);

  })
  return geos;
}






exports.geo = function(address) {

  var deferred = Q.defer();

  request('http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false&region=nz', function(error, res, body) {
    deferred.resolve(JSON.parse(body));
  })

  return deferred.promise;
}




var phoneFormat = function(phoneNum) {

  var formatted;

  var MOBILE = ['021', '022', '027'];

  var prefix = S(phoneNum).left(3).s;

  if (_.contains(MOBILE, prefix)) { // isMob

    // xxx-xxx-xxxx
    formatted = prefix + '-' + phoneNum.substr(3, 3) + '-' + S(phoneNum).chompLeft(phoneNum.substr(0, 6)).s;

  } else { // isTel

    // xx-xxx-xxxx
    formatted = S(phoneNum).left(2).s + '-' + phoneNum.substr(2, 3) + '-' + S(phoneNum).chompLeft(phoneNum.substr(0, 5)).s;
  }

  return formatted;
}





var saveShop = function(shopVars) {

  if (shopVars) {

    if (shopVars.isNotTemp) {
      var shop = new ShopModel(shopVars);

      shop.save(function(err, shop) {
        if (err) console.log(err);
        console.log(shop);
      })
    } else {
      var shop = new TmpShopModel(shopVars);

      shop.save(function(err, shop) {
        if (err) console.log(err);
        console.log(shop);
      })
    }


  }

}






exports = module.exports;
exports.html = '';