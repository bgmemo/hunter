'use strict';
/**
 *
 *
 * LONG STORIES HERE!!
 *
 *
 *
 */
var http = require('http');
var request = require('request');
var _ = require('lodash-node');
var C = require('cheerio');
var Q = require('q');
var S = require('string');
var V = require('validator');
var iconv = require('iconv-lite');
var async = require('async');

var ShopModel = require('./models/shop');
var TmpShopModel = require('./models/tmpshop');
var CategoryModel = require('./models/category');


/////////////// Defalt http options for mocha testing //////////////////
exports.host = 'localhost';
exports.port = 8888;
exports.basePath = '/b/l/restaurant/Auckland/'; //No num
exports.headers = {
  'User-Agent': 'Mozilla/6.0 (compatible; MSIE 7.0; Windows NT 6.0)'
}
////////////////////////////////////////////////////////////////////////


exports.NextPath = (function() {
  var i = 0,
    Next;

  Next = function() {
    i += 20;
  }

  Next.prototype.getPath = function(hasNext) {
    var path = (hasNext) ? (exports.basePath + i) : '';
    return path;
  }

  return Next;
}());


exports.prey = 0;


exports.sniff = function(currentPath) {
  if (!currentPath) {
    exports.stop();
  } else {
    var getChunkHtml = function(cb) {
      var options = {
        host: exports.host,
        port: exports.port,
        path: currentPath,
        headers: exports.headers,
        agent: false
      };

      http.get(options, function(res) {
        var size = 0;
        var chunks = [];
        res.on('data', function(chunk) {
          size += chunk.length;
          chunks.push(chunk);
        });
        res.on('end', function() {
          var data = Buffer.concat(chunks, size);
          var html = iconv.decode(data, 'utf-8'); //gb2312

          cb(null, html);
        });
      }).on('error', function(e) {
        console.log(e);
      });
    }

    var fns = [];
    fns.push(getChunkHtml);
    fns.push(exports.parseNsave);

    async.waterfall(fns, function(err, num) {
      // console.log("digest waterfall: " + num);

      var nextPath = new exports.NextPath();
      setTimeout(function() {
        exports.sniff(nextPath.getPath(exports.isFull(num)));
      }, 4000);

    })
  }
}



exports.parseNsave = function(html, cb) {
  var $ = C.load(html);
  var targetBlock = exports.shopDivs($);
  var name = targetBlock.first().children('a').first().find('span').text().substring(1, 3);
  var numShops = 0;

  var fetchCategory = function(cb) {
    CategoryModel.findOne({
      name: name
    }).exec().then(function(category) {
      cb(null, category);
    });
  }

  var ensureCategory = function(category, cb) {
    if (category) {
      cb(null, category);
    } else {
      var model = new CategoryModel();
      model.name = name;
      model.save(function(err, category) {
        cb(null, category);
      });
    }
  }


  var half_length = Math.ceil(targetBlock.length / 2) - 1;

  var left = targetBlock.splice(0, half_length);
  var right = targetBlock;

  var leftEach = function(category, cb) {
    setTimeout(function() {
    async.eachLimit(left, 11, function(block, callback) {
      if ($(block).is('li') && !$(block).hasClass('counter')) {
        numShops++;
        console.log(numShops);
        exports.parseShop($(block), category).then(function(vars) {
          saveShop(vars);
        })
      }
      callback();
    }, function(err) {
      cb(null, category)
    });
  }, 2000);
  }

  var rightEach = function(category, cb) {
    setTimeout(function() {
      async.eachLimit(right, 11, function(block, callback) {
        if ($(block).is('li') && !$(block).hasClass('counter')) {
          numShops++;
          console.log(numShops);
          exports.parseShop($(block), category).then(function(vars) {
            saveShop(vars);
          })
        }
        callback();
      }, function(err) {
        cb(null, numShops)
      });
    }, 2000);
  }

  var fns = [];

  fns.push(fetchCategory);
  fns.push(ensureCategory);
  fns.push(leftEach);
  fns.push(rightEach);

  async.waterfall(fns, function(err, numShops) {
    console.log("waterfall is working" + numShops);
    cb(null, numShops);
  });
}




var saveShop = function(shopVars) {
  if (shopVars) {
    if (shopVars.isNotTemp) {
      var shop = new ShopModel(shopVars);
      shop.save(function(err, shop) {
        if (err) console.log(err);
        console.log(shop.name);
      })
    } else {
      var shop = new TmpShopModel(shopVars);
      shop.save(function(err, shop) {
        if (err) console.log(err);
        console.log(shop.name);
      })
    }
  }
}




exports.parseShop = function(elem, category) {
  var deferred = Q.defer();
  var aList = elem.children('a');
  var shopName = aList.first().find('b').text();
  var shopPhone = V.whitelist(aList.last().text(), '0123456789');
  var shopCategory = category._id;

  aList.remove(); // For extracting address

  var shopAddress = S(elem.text()).humanize().s.substring(7);
  if (shopAddress.length > 18) {
    var shopVars = {
      name: shopName,
      phone: phoneFormat(shopPhone),
      _category: shopCategory,
      address: shopAddress,
      geo: {
        loc: {
          type: 'Point',
          coordinates: [0, 0]
        }
      }
    };
    exports.sortShop(shopVars).then(function(vars) {
      deferred.resolve(vars);
    })

  } else {
    console.log("shopVars.length: " + shopAddress.length);
    deferred.resolve('');
  }

  return deferred.promise;
}




exports.sortShop = function(vars) {
  var deferred = Q.defer();

  exports.geoFormat(vars.address).then(function(json) {
    var resultLength = json.results.length;
    if (resultLength > 1) {
      deferred.resolve(vars);
    } else {
      var shopVars = vars;
      var geos = exports.sortGeo(json.results);
      var geo = geos[0];
      var cord = [];
      cord.push(parseFloat(geo.lng));
      cord.push(parseFloat(geo.lat));

      shopVars.address = geo.street + ' ' + geo.route + ', ' + geo.district + ', ' + geo.city;

      shopVars.geo.street = geo.street;
      shopVars.geo.route = geo.route;
      shopVars.geo.district = geo.district;
      shopVars.geo.city = geo.city;
      shopVars.geo.loc.coordinates = cord;

      shopVars.isNotTemp = true;

      deferred.resolve(shopVars);
    }
  });

  return deferred.promise;
}




exports.geoFormat = function(address) {
  var deferred = Q.defer();

  var pool = new http.Agent();
  pool.maxSockets = 21;
  request({
    url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false&region=nz',
    pool: pool
  }, function(error, res, body) {
    deferred.resolve(JSON.parse(body));
  })

  return deferred.promise;
}



exports.shopDivs = function($) {
  var targetBlock = $('ul.result.fL').children();
  return targetBlock;
}



exports.sortGeo = function(results) {
  var geos = [];
  _(results).forEach(function(result) {
    var geo = {};
    var components = result.address_components;
    geo.lat = result.geometry.location.lat;
    geo.lng = result.geometry.location.lng;
    _(components).forEach(function(component) {
      var types = component.types;
      if (_.contains(types, 'street_number')) {
        geo.street = component.long_name;
      } else if (_.contains(types, 'route')) {
        geo.route = component.short_name;
      } else if (_.contains(types, 'sublocality')) {
        geo.district = component.long_name;
      } else if (_.contains(types, 'locality')) {
        geo.city = component.long_name;
      } else if (_.contains(types, 'postal_code')) {
        geo.zipcode = component.long_name;
      } else {}
    })

    geos.push(geo);
  })
  return geos;
}



var phoneFormat = function(phoneNum) {
  var formatted;
  var MOBILE = ['021', '022', '027'];
  var prefix = S(phoneNum).left(3).s;

  if (_.contains(MOBILE, prefix)) { // isMob
    // xxx-xxx-xxxx
    formatted = prefix + '-' + phoneNum.substr(3, 3) + '-' + S(phoneNum).chompLeft(phoneNum.substr(0, 6)).s;
  } else { // isTel
    // xx-xxx-xxxx
    formatted = S(phoneNum).left(2).s + '-' + phoneNum.substr(2, 3) + '-' + S(phoneNum).chompLeft(phoneNum.substr(0, 5)).s;
  }

  return formatted;
}


exports.isFull = function(num) {
  var isFull = (num === 20) ? true : false;
  return isFull;
}


exports.stop = function() {
  // if (process.env.NODE_ENV === 'production') {
  process.exit(1);
  // }
}

exports = module.exports;