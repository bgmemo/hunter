var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());


app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');
app.set('views', __dirname + '/app/views');
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
  res.render('index');
});

for (var i = 0, len = 20; i <= len; i += 20) {
  //0,20
  app.get('/b/l/restaurant/Auckland/' + i, function(req, res) {
    res.render('restaurant-full');
  });
}

app.get('/b/l/restaurant/Auckland/40', function(req, res) {
  res.render('restaurant-partial');
});

app.get('/b/l/restaurant/Auckland/60', function(req, res) {
  res.render('restaurant-empty');
});

app.listen(8888);